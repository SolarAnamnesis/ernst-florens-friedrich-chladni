# Über den Ursprung der von Pallas gefundenen und anderer ihr ähnlicher Eisenmassen, und über einige damit in Verbindung stehende Naturerscheinungen.

> Reul, C., Christ, D., Hartelt, A., Balbach, N., Wehner, M., Springmann, U., Wick, C., Grundig, Büttner, A., C., Puppe, F.: *OCR4all — An open-source tool providing a (semi-) automatic OCR workflow for historical printings* Applied Sciences **9**(22) (2019)

English - Plain Text  
English - PDF  
[German - Plain Text](full-text-german.md)  
[German - PDF](https://cdn.solaranamnesis.com/Chladni/chladni_pallas_eisenmassen_1794_german.pdf) | [Biolinum](https://cdn.solaranamnesis.com/Chladni/chladni_pallas_eisenmassen_1794_german_biolinum.pdf) | [Atkinson](https://cdn.solaranamnesis.com/Chladni/chladni_pallas_eisenmassen_1794_german_atkinson.pdf) | [Fraktur](https://cdn.solaranamnesis.com/Chladni/chladni_pallas_eisenmassen_1794_german_frak.pdf) | [Schwabacher](https://cdn.solaranamnesis.com/Chladni/chladni_pallas_eisenmassen_1794_german_swab.pdf)  
