# Ernst Chladni.

## Über den Ursprung der von Pallas gefundenen und anderer ihr ähnlicher Eisenmassen, und über einige damit in Verbindung stehende Naturerscheinungen.

> Reul, C., Christ, D., Hartelt, A., Balbach, N., Wehner, M., Springmann, U., Wick, C., Grundig, Büttner, A., C., Puppe, F.: *OCR4all — An open-source tool providing a (semi-) automatic OCR workflow for historical printings* Applied Sciences **9**(22) (2019)

English - Plain Text  
English - PDF  
[German - Plain Text](pallas-eisenmassen/full-text-german.md)  
[German - PDF](https://cdn.solaranamnesis.com/Chladni/chladni_pallas_eisenmassen_1794_german.pdf) | [Biolinum](https://cdn.solaranamnesis.com/Chladni/chladni_pallas_eisenmassen_1794_german_biolinum.pdf) | [Atkinson](https://cdn.solaranamnesis.com/Chladni/chladni_pallas_eisenmassen_1794_german_atkinson.pdf) | [Fraktur](https://cdn.solaranamnesis.com/Chladni/chladni_pallas_eisenmassen_1794_german_frak.pdf) | [Schwabacher](https://cdn.solaranamnesis.com/Chladni/chladni_pallas_eisenmassen_1794_german_swab.pdf)  

## Über Feuer-Meteore, und über die mit denselben herabgefallenen Massen.

Attempt at a translation of Ernst Chladni's work: "Über Feuer-Meteore, und über die mit denselben herabgefallenen Massen," from the original (Fraktur) German to English, I welcome all pull requests of submitted translations and/or edits.

Sections One through (most of) Three and Six were done manually. The other parts were done with the help of OCR4all:

> Reul, C., Christ, D., Hartelt, A., Balbach, N., Wehner, M., Springmann, U., Wick, C., Grundig, Büttner, A., C., Puppe, F.: *OCR4all — An open-source tool providing a (semi-) automatic OCR workflow for historical printings* Applied Sciences **9**(22) (2019)

[English - Plain Text](feuermeteor/full-text-english.md)  
English - PDF  
[German - Plain Text](feuermeteor/full-text-german.md)  
[German - PDF](https://cdn.solaranamnesis.com/Chladni/chladni_feuer_meteore_german-baskerville.pdf) | [Biolinum](https://cdn.solaranamnesis.com/Chladni/chladni_feuer_meteore_german-biolinum.pdf) | [Fraktur](https://cdn.solaranamnesis.com/Chladni/chladni_feuer_meteore_german-frak.pdf) | [Schwabacher](https://cdn.solaranamnesis.com/Chladni/chladni_feuer_meteore_german-swab.pdf)  
